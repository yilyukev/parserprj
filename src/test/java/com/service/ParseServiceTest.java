package com.service;

import com.service.exceptions.IncorrectFormatException;
import com.service.exceptions.UnsupportedDataTypeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class ParseServiceTest {

    @Autowired
    private ParseServiceImpl parseService;

    private static String correctParsingString = "(id,created,employee(id,firstname,employeeType(id), lastname),location)";
    private static String incorrectParsingString = "(id,created,employee(id,firstname,employeeType(id), lastname),location";

    private static String correctParsingResult = "id\n" +
            "created\n" +
            "employee\n" +
            "-id\n" +
            "-firstname\n" +
            "-employeeType\n" +
            "--id\n" +
            "- lastname\n" +
            "location\n";

    private static String correctParsingOrderedResult = "created\n" +
            "employee\n" +
            "- lastname\n" +
            "--id\n" +
            "-employeeType\n" +
            "-firstname\n" +
            "-id\n" +
            "id\n" +
            "location\n";

    @Test
    public void shouldReturnCorrectStringWhenInputParameterStringIsCorrect() {
        // Given
        String result = parseService.convertString(correctParsingString, Boolean.FALSE);

        // When Then
        assertThat(result).isEqualTo(correctParsingResult);
    }

    @Test
    public void shouldReturnCorrectOrderedStringWhenOrderFlagIsTrue()   {
        // Given
        String result = parseService.convertString(correctParsingString, Boolean.TRUE);

        // When Then
        assertThat(result).isEqualTo(correctParsingOrderedResult);
    }

    @Test
    public void shouldThrowExceptionWhenInputArrayContainsUnsupportedTypeObjects()  {
        // Given
        List<Object> tstList = new ArrayList<>(Arrays. asList("correctString", new BigDecimal(5)));

        // When Then
        Assertions.assertThrows(UnsupportedDataTypeException.class, () -> parseService.generateOutput(tstList, ""));
    }

    @Test
    public void shouldThrowExceptionWhenParsingStringHaveIncorrectStructure()   {
        // Given When Then
        Assertions.assertThrows(IncorrectFormatException.class, () -> parseService.convertString(incorrectParsingString, Boolean.FALSE));
    }

}
