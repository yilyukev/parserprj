package com.api;

import com.api.dto.RequestParseDto;
import com.service.ParseService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("/parser")
public class ApiEndpoint {

    @Autowired
    ParseService parseService;

    @PostMapping("/")
    public String parse(@RequestBody @Valid RequestParseDto request)   {
        return parseService.convertString(request.getInput(), request.isOrder());
    }
}
