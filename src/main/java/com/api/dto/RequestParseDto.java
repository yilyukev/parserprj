package com.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestParseDto {

    @Pattern(regexp = "\\((.*?)\\)")
    String input;

    boolean order;
}
