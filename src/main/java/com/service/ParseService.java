package com.service;

import com.service.exceptions.IncorrectFormatException;
import com.service.exceptions.UnsupportedDataTypeException;

public interface ParseService {

    String convertString(String str, boolean order) throws UnsupportedDataTypeException, IncorrectFormatException;

}
