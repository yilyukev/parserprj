package com.service.exceptions;

import static java.lang.String.format;

public class IncorrectFormatException extends RuntimeException {

    public IncorrectFormatException(String message) {
        super(message);
    }

    public IncorrectFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectFormatException(Throwable cause, String format, Object... args) {
        super(format(format, args), cause);
    }
}
