package com.service.exceptions;

import static java.lang.String.format;

public class UnsupportedDataTypeException extends RuntimeException {

    public UnsupportedDataTypeException(String message) {
        super(message);
    }

    public UnsupportedDataTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedDataTypeException(Throwable cause, String format, Object... args) {
        super(format(format, args), cause);
    }
}
