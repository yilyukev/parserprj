package com.service;

import com.service.exceptions.IncorrectFormatException;
import com.service.exceptions.UnsupportedDataTypeException;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@NoArgsConstructor
@Slf4j
public class ParseServiceImpl implements ParseService {

    @Override
    public String convertString(String str, boolean order) throws UnsupportedDataTypeException, IncorrectFormatException {

        if ( !validateStr(str) )    {
            throw new IncorrectFormatException("Format of parsing string incorrect.");
        }

        List<Object> objectList = parseString(str);

        if (order)  {
            objectList = sortList(objectList);
        }

        String res = generateOutput(objectList, "");

        return res;
    }

    private boolean validateStr(String str) {
        log.debug("Validation string: {}", str);
        Pattern pattern = Pattern.compile("\\((.*?)\\)");
        Matcher matcher = pattern.matcher(str);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    List<Object> parseString(String str)    {
        List<Object> resultList = new ArrayList<>();
        StringBuilder word = new StringBuilder();

        // remove first '(' and last ')' symbols
        str = str.substring(str.indexOf("(") + 1, str.lastIndexOf(")"));

        for (int i = 0; i < str.length(); i++)  {
            switch (str.charAt(i))  {
                case '(':
                    if (!StringUtils.isEmpty(word.toString())) {
                        resultList.add(word.toString());
                        word = new StringBuilder();
                    }
                    String subStr = str.substring(str.indexOf("("), str.lastIndexOf(")") + 1);
                    resultList.add(parseString(subStr));
                    i = i + subStr.length();
                    break;

                case ',':
                    resultList.add(word.toString());
                    word = new StringBuilder();
                    break;

                case ' ':
                    // do nothing
                    break;

                default:
                    word.append(str.charAt(i));
            }
        }

        if (!StringUtils.isEmpty(word.toString()))  {
            resultList.add(word.toString());
        }

        return resultList;
    }


    String generateOutput(List<Object> list, String prefix) throws UnsupportedDataTypeException  {
        StringBuilder result = new StringBuilder();
        for (Object val: list) {
            switch (val.getClass().getCanonicalName()) {
                case "java.lang.String":
                    result.append(prefix).append(val).append("\n");
                    break;
                case "java.util.ArrayList":
                    String s = generateOutput((List<Object>) val, prefix + "-");
                    result.append(s);
                    break;
                default:
                    throw new UnsupportedDataTypeException("Unsupported data type in parse process.");
            }
        }
        return result.toString();
    }

    private List<Object> sortList(List<Object> list)    {
        Object tmp;
        String a;
        String b;

        log.debug("Sorted enable");

        for (int i=0; i < list.size(); i++) {
           if (list.get(i).getClass().getCanonicalName().equals("java.lang.String"))    {

               for (int j=i+1; j < list.size(); j++)  {
                   if (list.get(j).getClass().getCanonicalName().equals("java.lang.String"))    {
                       a = (String) list.get(i);
                       b = (String) list.get(j);

                       if (a.compareTo(b) > 0) {
                            list.set(i, b);
                            list.set(j, a);
                            if (j+1 < list.size() && list.get(j+1).getClass().getCanonicalName().equals("java.util.ArrayList"))  {
                                tmp = list.get(i+1);
                                list.set(i+1, list.get(j+1));
                                list.set(j+1, tmp);
                            }
                       }

                   }
               }

           }    else    {
               sortList((List<Object>) list.get(i));
           }
        }
        return list;
    }

}
